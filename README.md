# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Пример кода. онлайн-конвертер рубль - японская йена
* 01
* Приложение принимает запросы и отдает ответы в следующем формате:
* 1. Покупка йен за рубли
* Запрос: тип запроса - GET, параметр sum - сумма в рублях, положительное десятичное число с разделителем “.”, точность дробной части - 2 знака.
* http://localhost:8080/yconverter/buy?sum=123.45
* Ответ: JSON-формат, sum - сумма в йенах, число, формат тот же, что в запросе, status - строка, “ОК” в случае успешного ответа, “ERROR” в случае ошибки.
* { sum: 67.89, status: “OK”}
* 2. Продажа йен за рубли
* Запрос: тип запроса - GET, параметр sum - сумма в йенах, положительное десятичное число с разделителем “.”, точность дробной части - 2 знака.
* http://localhost:8080/yconverter/sell?sum=333.44
* Ответ: JSON-формат, sum - сумма в рублях, число, формат тот же, что в запросе, status - строка, “ОК” в случае успешного ответа, “ERROR” в случае ошибки.
* { sum: 555.66, status: “OK”}
* Курс йены запрашивается с сайта ЦБ РФ при запуске приложения и далее с периодичностью один раз в час. 
* !!! ПОКА НЕ РЕАЛИЗОВАНО: Полученный курс сохранять в базе данных с указанием времени его получения. !!! На данный момент курс запрашивается при каждом запросе к сервису.
* !!! ПОКА НЕ РЕАЛИЗОВАНО: При запросе на покупку или продажу использовать актуальный (последний по времени получения) курс, сохраненный в БД.
* Адрес для получения курса ЦБ РФ: http://www.cbr.ru/scripts/XML_daily.asp



### How do I get set up? ###
1. Проверить и изменить параметры подключения к БД в файле config.properties в коневом каталоге.
    Значения по умолчанию:
        connection_URL=jdbc:mysql://localost:3306/myConnection
        jdbc_driver_class_name=com.mysql.jdbc.Driver
        connection_user = root
        connection_password = root

2. Таблица в базе данных создаётся при запуске сервиса и начинает один раз в час (начиная с момента создания) добавлять
туда записи с севиса ЦБ.

3. Собрать проект командой mvn clean install. Результат - папка target, в корне которой находится файл ConverterRUBtoJPY-1.0.war

4. Копируем указанный выше файл в папку \webapps\ директории, где расположен Tomcat

5. Запускаем Tomcat

6. Ссылки для работы с сервисом через бд:
    http://localhost:8080/yconverter/buy?sum=123.45
    http://localhost:8080/yconverter/sell?sum=333.44
7. Ссылки для работы с сервисом без бд, запрашивая курс напрямую у ЦБ (изменить хост и порт в зависимости от параметров
сервера):
    http://localhost:8080/yconverter/witoutdb/buy?sum=123.45
    http://localhost:8080/yconverter/witoutdb/sell?sum=333.44



### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact