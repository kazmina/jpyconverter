package webservice;

import cbrClient.CbrClientImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.tree.DefaultDocument;
import utils.RateGettingUtils;
import utils.StatusList;
import utils.WSResponce;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Service implementation, than takes rate directly from cbr service
 * Created by tatiana.kazmina on 12.01.2018.
 */
@Path("/witoutdb")
public class ConverterServiceFromCBDirectlyImpl implements ConverterService{
    private static final Logger LOGGER = LogManager.getLogger(ConverterServiceFromCBDirectlyImpl.class.getName());
    private RateGettingUtils rateUtil = new RateGettingUtils();
    @GET
    @Path("/buy")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    public WSResponce buyJPY(@PathParam("sum") String sumRUB) {
        WSResponce responce = new WSResponce();
        double rate = rateUtil.getRateFromXML();
        if (rate == 0) {
            responce.setStatus(StatusList.ERROR);
            responce.setSum(0);
            return responce;
        } else {
            double sumR = Double.parseDouble(sumRUB);
            responce.setSum(sumR / rate);
            //TODO: format output for rate - only 2 digits after the decimal point
            responce.setStatus(StatusList.OK);
        }
        return responce;
    }


    @GET
    @Path("/sell")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    public WSResponce sellJPY(@PathParam("sum")String sumJPY) {
        WSResponce responce = new WSResponce();
        double rate = rateUtil.getRateFromXML();
        if (rate == 0) {
            responce.setStatus(StatusList.ERROR);
            responce.setSum(0);
            return responce;
        } else {
            double sumJ = Double.parseDouble(sumJPY);
            responce.setSum(sumJ * rate);
            //TODO: format output for rate - only 2 digits after the decimal point
            responce.setStatus(StatusList.OK);
        }
        return responce;
    }
}

