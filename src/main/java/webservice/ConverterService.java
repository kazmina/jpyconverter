package webservice;

import utils.WSResponce;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


/**
 * Interfact for web-service with 2 web-methods to convert RUB to JPY and JPY to RUB
 * Created by tatiana.kazmina on 12.01.2018.
 */

public interface ConverterService {

    WSResponce buyJPY (String sumRUB);

    WSResponce sellJPY (String sumJPY);
}
