package webservice;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import utils.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Service implementation, that takes rate values from db
 * Created by tatiana.kazmina on 17.01.2018.
 */
@Path("/")
public class ConvertertServiceWithDBImpl implements ConverterService {
    private static final Logger SERV_DB_LOG = LogManager.getLogger(ConvertertServiceWithDBImpl.class.getName());
    private RateGettingUtils rateUtil;
    private ScheduledExecutorService scheduler;
    private RunnableUtils runnable;
    Dao dao;

    /**
     * Scheduler starts when service's object is created.
     */
    public ConvertertServiceWithDBImpl() {
        rateUtil = new RateGettingUtils();
        scheduler = Executors.newScheduledThreadPool(1);
        dao = new DaoImpl();
        dao.createTable();
        runnable = new RunnableUtils(dao);
        scheduler.scheduleAtFixedRate(runnable, 0, 1, TimeUnit.HOURS);
    }

    @GET
    @Path("/buy")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    public WSResponce buyJPY(@PathParam("sum")String sumRUB) {
        WSResponce responce = new WSResponce();
        double rate = rateUtil.getRateFromDB(getDao());
        if (rate == 0) {
            responce.setStatus(StatusList.ERROR);
            responce.setSum(0);
            return responce;
        } else {
            double sumR = Double.parseDouble(sumRUB);
            responce.setSum(sumR / rate);
            //TODO: format output for rate - only 2 digits after the decimal point
            responce.setStatus(StatusList.OK);
        }
        return responce;
    }

    @GET
    @Path("/sell")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    public WSResponce sellJPY(@PathParam("sum")String sumJPY) {
        WSResponce responce = new WSResponce();
        double rate = rateUtil.getRateFromDB(getDao());
        if (rate == 0) {
            responce.setStatus(StatusList.ERROR);
            responce.setSum(0);
            return responce;
        } else {
            double sumJ = Double.parseDouble(sumJPY);
            responce.setSum(sumJ * rate);
            //TODO: format output for rate - only 2 digits after the decimal point
            responce.setStatus(StatusList.OK);
        }
        return responce;
    }

    public Dao getDao() {
        return dao;
    }
}
