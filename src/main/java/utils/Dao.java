package utils;

/**
 * Created by tatiana.kazmina on 17.01.2018.
 */
public interface Dao {
    public void createTable();
    public double getRateFromDB();
    public void addRowToDBTable();
}
