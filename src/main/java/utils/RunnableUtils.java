package utils;

/**
 * Created by tatiana.kazmina on 17.01.2018.
 */
public class RunnableUtils implements Runnable {
    Dao dao;

    public RunnableUtils(Dao dao) {
        this.dao = dao;
    }

    /**
     * Method for executing within scheduler 1 time per hour.
     */
    public void run() {
       dao = new DaoImpl();
       dao.addRowToDBTable();
    }
}
