package utils;

import cbrClient.CbrClientImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.tree.DefaultDocument;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Created by tatiana.kazmina on 17.01.2018.
 */
public class RateGettingUtils {
    private static final Logger RATE_UTIL_LOG = LogManager.getLogger(RateGettingUtils.class.getName());

    /**
     * Method for getting exchangeRate from XML Document.
     * @return int rate - how many RUB is the one JPY.
     */
    public double getRateFromXML () {

        Document xmlRates = new DefaultDocument();
        double rate = 0;
        try {
            xmlRates = CbrClientImpl.getInstance().getXMLrates();
        } catch (Exception ex) {
            StringWriter stack = new StringWriter();
            ex.printStackTrace(new PrintWriter(stack));
            RATE_UTIL_LOG.error("Error while XML recieving. " + stack.toString());
        }
        Element root = xmlRates.getRootElement();
        java.util.List<Node> nodeList = xmlRates.selectNodes("/ValCurs/Valutes");
        for (Node node : nodeList) {
            if (node.valueOf("@ID").equals("R01820")) {
                String rateValTxt = node.selectSingleNode("Value").getText();
                int rateVal = Integer.parseInt(rateValTxt);
                String nominalTxt = node.selectSingleNode("Nominal").getText();
                int nominal = Integer.parseInt(nominalTxt);
                rate = rateVal / nominal;
            }
        }
        return rate;
    }

    /**
     * Method for getting exchangeRate from Database.
     * @return int rate - how many RUB is the one JPY.
     */
    public double getRateFromDB(Dao dao) {
        double rate = dao.getRateFromDB();
        return rate;
    }
}
