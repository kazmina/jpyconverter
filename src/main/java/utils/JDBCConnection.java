package utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

/**
 * Created by tatiana.kazmina on 17.01.2018.
 */
public class JDBCConnection {
    private static final Logger JDBC_CONN_LOGGER = LogManager.getLogger(JDBCConnection.class.getName());

    public Connection getConnection() {
        Connection connection = null;
        Properties props = new Properties();
        InputStream input = null;
        String connectionURL = null;
        String jdbcDriverClassName = null;
        String connectionPassword = null;
        String connectionUser = null;
        try {

            String filename = "config.properties";
            input = getClass().getClassLoader().getResourceAsStream(filename);
            if(input==null){
                JDBC_CONN_LOGGER.error("Can't find config.properties on classpath!");
                return null;
            }

            props.load(input);

            connectionURL = props.getProperty("connection_URL");
            jdbcDriverClassName = props.getProperty("jdbc_driver_class_name");
            connectionPassword = props.getProperty("connection_password");
            connectionUser = props.getProperty("connection_user");

        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            try {
                Class.forName(jdbcDriverClassName).newInstance();
                connection = DriverManager.getConnection(connectionURL, connectionUser, connectionPassword);
            } catch (Exception e) {
                JDBC_CONN_LOGGER.error(e.getMessage());
            }
            return connection;
        }
    }
}

/*Команда создания таблицы в БД:
* CREATE TABLE datesAndRates(
*   id INT(5) AUTO_INCREMENT,
*   date DATE NOT NULL,
*   rate DECIMAL(5,2) NOT NULL,
*   PRIMARY KEY (id)
*   );
*   */
