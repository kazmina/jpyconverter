package utils;

/**
 * Created by tatiana.kazmina on 15.01.2018.
 */
public class WSResponce {
    private double sum;
    private StatusList status;

    public double getSum() {
        return sum;
    }

    public StatusList getStatus() {
        return status;
    }

    public void setSum(double sum) {
        this.sum = sum;
    }

    public void setStatus(StatusList status) {
        this.status = status;
    }
}
